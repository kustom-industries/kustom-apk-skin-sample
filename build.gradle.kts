buildscript {
    repositories {
        mavenCentral()
        google()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:8.5.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.23")
    }
}

allprojects {
    allprojects {
        repositories {
            mavenCentral()
            google()
        }
        ext {
            set("buildToolsVersion", "35.0.0")
            set("compileSdkVersion", 35)
            set("targetSdkVersion", 35)
        }
    }
}

